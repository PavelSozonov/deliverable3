package innopolis.poporo;

public class EnchantedPage extends Page {

	public EnchantedPage() {
	}
	
	public EnchantedPage(ContentItem c) {
		content.add(c);
	}
	
	@Override
	public void upload(ContentItem c) {
		if (!content.contains(c))
			content.add(c);
	}

	@Override
	public boolean remove(ContentItem c) {
		if (content.size() > 1) {
			if (content.contains(c)) {
				c.removeAllComments();
				content.remove(c);
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	@Override
	public boolean isEmptyPage() {
		return content.isEmpty();
	}

	@Override
	public boolean containsContentItem(ContentItem c) {
		return content.contains(c);
	}

	@Override
	public void Draw() {
		System.out.println("Enchanted page Draw()");		
	}

}

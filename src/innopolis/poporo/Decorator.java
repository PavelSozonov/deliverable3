package innopolis.poporo;

public abstract class Decorator extends Page {
	protected Page page;
	
	public Decorator(Page p) {
		page = p;
	}
	
	@Override
	public void Draw() {
		page.Draw();
	}

	@Override
	public void upload(ContentItem c) {
		page.upload(c);
	}

	@Override
	public boolean remove(ContentItem c) {
		return page.remove(c);
	}

	@Override
	public boolean isEmptyPage() {
		return page.isEmptyPage();
	}

	@Override
	public boolean containsContentItem(ContentItem c) {
		return page.containsContentItem(c);
	}
}

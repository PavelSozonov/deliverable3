package innopolis.poporo;

import java.util.LinkedList;
import java.util.List;

public class ContentItem {
	protected List<ContentItem> comments = new LinkedList<ContentItem>();
	
	public void addComment(ContentItem c) {
		comments.add(c);
	}
	
	public void removeComment(ContentItem c) {
		if (comments.contains(c))
			comments.remove(c);
	}
	
	public boolean has(ContentItem c) {
		return comments.contains(c);
	}
	
	public boolean hasComments() {
		if (comments.size() > 0) return true;
		return false;
	}
	
	public void removeAllComments() {
		comments = new LinkedList<ContentItem>();
	}
}

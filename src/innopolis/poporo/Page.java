package innopolis.poporo;

import java.util.LinkedList;
import java.util.List;

public abstract class Page {
	protected User owner = new User();
	protected List<ContentItem> content = new LinkedList<ContentItem>();
	
	public abstract void upload(ContentItem c);
	public abstract boolean remove(ContentItem c);
	public abstract boolean isEmptyPage();
	public abstract boolean containsContentItem(ContentItem c);
	public abstract void Draw();
}

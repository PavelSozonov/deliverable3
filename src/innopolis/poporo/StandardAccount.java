package innopolis.poporo;

public class StandardAccount extends Account {

	@Override
	public User openAccount() {
		setIsOpen(true);
		User u = new User();
		page = new StandardPage();
		return u;
	}

	@Override
	public void closeAccount() {
		setIsOpen(false);
		page = null;
	}

	@Override
	public void transmit(ContentItem c) {
		if (getIsOpen())
			page.upload(c);
	}

	@Override
	public boolean contains(ContentItem c) {
		if (getIsOpen())
			return page.containsContentItem(c);
		else 
			return false;
	}
}

package innopolis.poporo;

public class StandardPoporoFactory extends PoporoFactory {

	@Override
	public ContentItem createContentItem() {
		StandardContentItem sci = new StandardContentItem();
		return sci;
	}

	@Override
	public User createUser() {
		StandardUser su = new StandardUser();
		return su;
	}

	@Override
	public Page createPage() {
		StandardPage sp = new StandardPage();
		return sp;
	}

	@Override
	public Account createAccount() {
		StandardAccount sa = new StandardAccount();
		return sa;
	}

}
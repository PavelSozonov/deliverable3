package innopolis.poporo;

public abstract class Account {
	protected User user;
	protected Page page;
	protected boolean isOpen = false;
	
	public void setIsOpen(boolean b) {
		isOpen = b;
	}
	
	public boolean getIsOpen() {
		return isOpen;
	}
	
	public abstract User openAccount();
	public abstract void closeAccount();
	public abstract void transmit(ContentItem c);
	public abstract boolean contains(ContentItem c);
}

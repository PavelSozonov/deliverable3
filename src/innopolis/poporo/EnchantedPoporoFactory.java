package innopolis.poporo;

public class EnchantedPoporoFactory extends PoporoFactory {

	@Override
	public ContentItem createContentItem() {
		EnchantedContentItem eci = new EnchantedContentItem();
		return eci;
	}

	@Override
	public User createUser() {
		EnchantedUser eu = new EnchantedUser();
		return eu;
	}

	@Override
	public Page createPage() {
		EnchantedPage ep = new EnchantedPage();
		return ep;
	}

	@Override
	public Account createAccount() {
		EnchantedAccount ea = new EnchantedAccount();
		return ea;
	}

}

package innopolis.poporo;

public class WallDecorator extends Decorator {
	
	public WallDecorator(Page p) {
		super(p);
	}
	
	@Override
	public void Draw() {
		System.out.println(" Draw in class WallDecorator ");
		super.Draw();
		DrawWall();
	}
	
	public void DrawWall() {
		System.out.println("Draw wall");
	}
	
	public void comment(ContentItem c1, ContentItem c2) {
		if (c1 != null) 
			c1.addComment(c2);
	}
	
	public void uncomment(ContentItem c1, ContentItem c2) {
		if (c1 != null)
			c1.removeComment(c2);
	}
	
	public boolean has(ContentItem c1, ContentItem c2) {
		if (c1 != null)
			return c1.has(c2);
		return false;
	}

	public boolean isDefinedAt(ContentItem c1) {
		if (c1 != null)
			return c1.hasComments();
		return false;
	}
}
